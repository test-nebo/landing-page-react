# Тестовое задание

## решение

- для сборки использую подход docker-in-docker

    <details><summary>для ускорения pipline желательно использовать свой runner</summary>

          при сборке слои будут использоваться повторно.
          для возможности параллельного выполнения поднимаю concurrent

          concurrent = 3
          check_interval = 0
          shutdown_timeout = 0

          [session_server]
            session_timeout = 1800

          [[runners]]
            name = "nebo-test-runner"
            url = "https://gitlab.com/"
            id = 28139696
            token = "*************************"
            token_obtained_at = 2023-10-03T08:06:08Z
            token_expires_at = 0001-01-01T00:00:00Z
            executor = "docker"
            [runners.cache]
              MaxUploadedArchiveSize = 0
            [runners.docker]
              tls_verify = false
              image = "docker:stable"
              privileged = false
              disable_entrypoint_overwrite = false
              oom_kill_disable = false
              disable_cache = false
              volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
              shm_size = 0

  </details>

- для уменьшения размера deployImage беру nginx:alpine
- для решения IMAGE_NAME использую bash

## что можно улучшить

- сохранять образы в registry, использовать повторно + kubernetes
- дописать docker-compose

## тз

- Необходимо разработатать CI\CD окружение для проекта на гитлаб.
- Необходимо создать проект на https://gitlab.com/

CI\CD должно состоять из 2х стадий со следующими задачами:

- Build
  o buildImage
- Deploy
  o deployImage
  o deployImageCustom
  o sleep

Описание задач:

• buildImage - Необходимо создать докер образ, который бы разворачивал любое статичное web приложение(Например: https://github.com/sujoyduttajad/Landing-Page-React). Для установки зависимостей - npm install. Для запуска сервера можно воспользоваться https://www.npmjs.com/package/serve или любым другим веб-сервером
• deployImage - Образ получившийся в buildImage необходимо залить в файловое хранилище с помощью curl (реальный запрос может не выполняться, для примера можно указать любой url)
• deployImageCustom - Запускается вручную и может принимать переменную IMAGE_NAME. Если эта переменная указана, необходимо переименовать образ в значение этой переменной. Если не указана, название не переименовывается. Образ также должен быть задеплоен с помощью curl.
• Sleep - Задача заглушка, имеющая в себе только sleep на 2 минуты.

Все задачи кроме deployImageCustom должны выполняться при мерже в main ветку. Pipeline должен быть оптимизирован и занимать как можно меньше времени раннеров.
