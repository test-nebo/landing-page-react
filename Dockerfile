ARG ALPINE_VERSION=3.18
ARG NGINX_VERSION=1.25.2
ARG NODE_ENV=20.8.0


# ----------- Build stage ------------
FROM node:${NODE_ENV}-alpine${ALPINE_VERSION} as builder
WORKDIR '/app'
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build


# ----------- Run stage ------------
FROM nginx:${NGINX_VERSION}-alpine${ALPINE_VERSION}-slim as runner
WORKDIR '/usr/share/nginx/html'
RUN rm -rf ./*
COPY --from=builder /app/build .

ENTRYPOINT ["nginx", "-g", "daemon off;"]
